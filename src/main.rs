use std::fs::OpenOptions;
use std::process::Command;
use std::io::Write;
use std::env;
use fs2::FileExt;

fn print_results(router: &str, out_fname: &str, out: Vec<u8>) {
    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_reader(out.as_slice());
    let mut record = csv::StringRecord::new();
    let mut results = String::new();
    loop {
        if !rdr.read_record(&mut record).unwrap() {
            break;
        }
        let ts = (&record[0]).trim();
        let inidx = (&record[1]).trim();
        let outidx = (&record[2]).trim();
        let sp = (&record[3]).trim();
        let dp = (&record[4]).trim();
        let sn = (&record[5]).trim();
        let dn = (&record[6]).trim();
        let sas = (&record[7]).trim();
        let das = (&record[8]).trim();
        let byt = (&record[9]).trim();
        let mut peer = "";
        let mut ingress = false;
        if router == "LAX-AGG8" {
            if inidx == "122" {
                peer = "TRCPS";
                ingress = true;
            } else if outidx == "122" {
                peer = "TRCPS";
            } else if inidx == "430" {
                peer = "Microsoft";
                ingress = true;
            } else if outidx == "430" {
                peer = "Microsoft";
            } else if inidx == "437" || inidx == "443" {
                peer = "AWS";
                ingress = true;
            } else if outidx == "437" || outidx == "443" {
                peer = "AWS";
            } else {
                continue
            }
        }
        if router == "SVL-AGG8" {
            if inidx == "369" {
                peer = "TRCPS";
                ingress = true;
            } else if outidx == "369" {
                peer = "TRCPS";
            } else if inidx == "368" {
                peer = "Microsoft";
                ingress = true;
            } else if outidx == "368" {
                peer = "Microsoft";
            } else if inidx == "92" {
                peer = "AWS";
                ingress = true;
            } else if outidx == "92" {
                peer = "AWS";
            } else {
                continue
            }
        }
        if router == "OAK-AGG4" {
            if inidx == "192" {
                peer = "Google";
                ingress = true;
            } else if outidx == "192" {
                peer = "Google";
            } else if inidx == "322" {
                peer = "AWS";
                ingress = true;
            } else if outidx == "322" {
                peer = "AWS";
            } else {
                continue
            }
        }
        if router == "SVL-AGG4" {
            if inidx == "244" {
                peer = "Google";
                ingress = true;
            } else if outidx == "244" {
                peer = "Google";
            } else if inidx == "301" {
                peer = "AWS";
                ingress = true;
            } else if outidx == "301" {
                peer = "AWS";
            } else {
                continue
            }
        }
        if router == "LAX-AGG6" {
            if inidx == "523" || inidx == "664" {
                peer = "Google";
                ingress = true;
            } else if outidx == "523" || outidx == "664" {
                peer = "Google";
            } else {
                continue
            }
        }
        if peer == "" {
            continue
        }
        results.push_str(format!(
            "{},{},{},{},{},{},{},{},{},{},{},{}\n",
            ts, inidx, outidx, sp, dp, sn, dn, sas, das, byt, peer, ingress
        ).as_str())
    }
    let mut file = OpenOptions::new()
        .append(true)
        .create(true)
        .open(out_fname)
        .unwrap();
    file.lock_exclusive().unwrap();
    file.write_all(results.as_bytes()).unwrap();
    file.unlock().unwrap();
}

fn main() {
    let args: Vec<String> = env::args().collect();
    assert!(args.len() == 4);
    let router = &args[1];
    let in_fname = &args[2];
    let out_fname = &args[3];
    let output = Command::new("nfdump").arg("-Nq")
        .arg("-o").arg("fmt:%ts,%in,%out,%sp,%dp,%sn,%dn,%sas,%das,%byt")
        .arg("-r").arg(in_fname)
        .output().unwrap();
    assert!(output.status.success());
    print_results(router, out_fname, output.stdout);
}
